clc; close all; clear all;
addpath(genpath('../mr-fingerprinting-scripts/MRF recon scripts'));
dcmDir='DICOM';
RenameAndExportExamCardFromDicom (dcmDir, 1, 1, 0, 1, 0) %non recursive folder search you can change this
files=scanInfo(dcmDir);
% load('zzz_Phantom_MarsLab_063_09_01_MRF RAFF 1 slice 500pause_XXscanpars.mat');
MRF_ind = [];
for i = 1:length(files)
    if files(i).MRF == 1
        MRF_ind = [MRF_ind,i];
    end
end
files_MRF = files(MRF_ind)
gpu=1;
try
    gpuDevice()
catch
end
disp('cpu info:')
try
    cpuinfo()
catch
end
%%
increment=5;
output_dir=['output_', datestr(now,'mm-dd-yyyy_HH-MM-ss')];
mkdir(output_dir);
%%
for ind=1:length(files_MRF)
    try
        %%
        XXinfo=files_MRF(ind).XXinfo;
        [data_rescaled,MRF_dicom_info]=loadPhillipsDicom([files_MRF(ind).folder,'/',files_MRF(ind).name]);
        
        
        %%
        
        
        [dict_input]=create_dict_input(increment,XXinfo,MRF_dicom_info)
        %%
        %         if size(data_rescaled,3)~=(double(dict_input.N_slice).*length(dict_input.TR))
        %             disp('data size not consistent with N_slice*N_dyn');
        %             error('data size not consistent with N_slice*N_dyn')
        %         end
        %%
        tic()
        if dict_input.new_patch==0
            [dict] = DictM2DMRFrepp(dict_input.RFpulses, dict_input.RefDic ,dict_input.TR , dict_input.TE,dict_input.Slice_Order,dict_input.extra_inversion,dict_input.break_MRF,dict_input.REPP_times,dict_input.N_no_SL,dict_input.SL_wait,dict_input.M2D_jump,dict_input.extra_repp_factor,dict_input.REPP_in_2nd);
            
        else
            [dict] = DictM2DMRFrepp_new(dict_input.RFpulses, dict_input.RefDic ,dict_input.TR , dict_input.TE,dict_input.Slice_Order,dict_input.extra_inversion,dict_input.break_MRF,dict_input.REPP_times,dict_input.N_no_SL,dict_input.SL_wait,dict_input.M2D_jump,dict_input.extra_repp_factor,dict_input.shuffle);
        end
        disp('Dictionary generation took:')
        toc()
        
        %%
        N_dyn_recon=floor(size(data_rescaled,3)/double(dict_input.N_slice));
        %%
        data_rescaled(:,:,MRF_dicom_info.dyn_slice(3,:)>N_dyn_recon)=[];
        MRF_dicom_info.dyn_slice(:,MRF_dicom_info.dyn_slice(3,:)>N_dyn_recon)=[];
        %%
        dict=dict(:,1:N_dyn_recon,:);
        %%
        T1=zeros(size(data_rescaled,1),size(data_rescaled,2),dict_input.N_slice);
        T2star=T1;
        Trepp=T1;
        MaxCorrVals=T1;
        CorrelationCoeffs=T1;
        AngleCorr=T1;
        
        D=abs(dict);
        tic()
        
        for slice=1:dict_input.N_slice
            disp(['matching slice:',num2str(slice),'/',num2str(dict_input.N_slice)])
            dyn_slice = MRF_dicom_info.dyn_slice;
            slice_index = dyn_slice(2,:);
            image = data_rescaled(:,:, slice_index==slice);
            %             disp(size(image))
            %             disp(size(D))
            if gpu==1
                [MaxCorrVal, CorrelationCoeff]=MatchMRF_gpu(D(:,:,slice),image,200);
            else
                [MaxCorrVal, CorrelationCoeff]=MatchMRF(D(:,:,slice),image);
            end
            for i=1:size(image,1)
                for j=1:size(image,2)
                    T1(i,j,slice)=dict_input.RefDic(1,MaxCorrVal(i,j));
                    T2star(i,j,slice)=dict_input.RefDic(2,MaxCorrVal(i,j));
                    Trepp(i,j,slice)=dict_input.RefDic(5,MaxCorrVal(i,j));
                    AngleCorr(i,j,slice)=dict_input.RefDic(3,MaxCorrVal(i,j));
                    
                end
            end
            MaxCorrVals(:,:,slice)=MaxCorrVal;
            CorrelationCoeffs(:,:,slice)=CorrelationCoeff;
            %
            
            
        end
        disp('Matching took:')
        toc()
        Map.T1=T1;
        Map.T2star=T2star;
        Map.Trepp=Trepp;
        Map.AngleCorr=AngleCorr;
        Map.MaxCorrVal=MaxCorrVals;
        Map.CorrelationCoeff=CorrelationCoeffs;
        Map.dict_input=dict_input;
        save([output_dir,'/Map_incr_',num2str(increment),'_',files_MRF(ind).name(1:end-4),'.mat'],'Map');
        clear dict
        clear D
    catch e %e is an MException struct
        
        disp(['error while mathing: ', files_MRF(ind).name(1:end-4)]);
        disp(['The identifier was:\n%s',e.identifier]);
        %         fprintf(1,'The identifier was:\n%s',e.identifier);
        %                 fprintf(1,'There was an error! The message was:\n%s',e.message);
    end
end
