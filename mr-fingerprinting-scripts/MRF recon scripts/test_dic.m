
RFpulses=1:55;
RefDic=createREFDic(1,60);
TR=RFpulses+100;
TE=RFpulses;
slice_order=[1:30];
extra_inversion=1;
prescan_wait=1;
REPP_times=1:10;
N_no_SL=35;
SL_wait=0;
M2D_jump=7;
extra_REPP_fact=2;
[dict] = DictM2DMRFrepp(RFpulses, RefDic ,TR , TE,slice_order,extra_inversion,prescan_wait,REPP_times,N_no_SL,SL_wait,M2D_jump,extra_REPP_fact);
%%
N_atoms=1e6;
N_slice=1;
M0 = rand(3,N_atoms,N_slice);
TR=40;
T1=800;
T2star=30;
%%
M0_out=zeros(size(M0));
tic()
for i=1:20
for k=1:N_slice
    M0_out(:,:,k) = relx3D(M0(:,:,k) , TR, T1, T2star);
end
end
toc()

%%
tic()
for i=1:20
M_out=relx3D_faster(M0, TR, T1, T2star);
end
toc()

%%
%%
function m_out = relx3D_faster(m_in, TE_in, T1_in, T2_in)
% relax vecot by T1 T2
e1te = exp(-TE_in./T1_in);        % relaxation terms
e2te = exp(-TE_in./T2_in);

m_out = zeros(size(m_in));

m_out(1,:,:) = m_in(1,:,:).*e2te;
m_out(2,:,:) = m_in(2,:,:).*e2te;
m_out(3,:,:) = m_in(3,:,:).*e1te + 1 - e1te ;
end


%%