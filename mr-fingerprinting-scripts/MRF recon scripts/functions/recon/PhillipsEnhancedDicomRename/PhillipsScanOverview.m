function [files] = PhillipsScanOverview()
%PHILLIPSSCANOVERVIEW Function to convert your enhanced DICOM to usable
%filenames and create a fileoverview: to extract your custom paramter
%IMPORTANT: Please read and change scanInfo.mat to include you custom paramters
%Outputs a structure containing all saved info about your scans.
%This overview is also saved in the original folder toghether with an easy
%to read excel file.
% EXCEED is from paradise forum
%Code could be buggy so use at own risk
% % Author: Telly Ploem
% Date: 12-2020

proc = input('Data already processed by EXCEED (so renamed and mat files) \n please enter y or n: ','s');
if proc=='y'
    MRFdir=uigetdir('open dicom folder');
    files=scanInfo(MRFdir);
    disp('done')
end
if proc=='n'
    MRFdir=uigetdir('open dicom folder');
    disp('Start renaming files')
    RenameAndExportExamCardFromDicom (MRFdir, 1, 1, 0, 1, 0) %non recursive folder search you can change this
    files=scanInfo(MRFdir);
    disp('done')
else
    disp('No action please enter y or n next time')
end

end

