function varargout = EXCEED(varargin)
% EXCEED MATLAB code for EXCEED.fig
%      EXCEED, by itself, creates a new EXCEED or raises the existing
%      singleton*.
%
%      H = EXCEED returns the handle to a new EXCEED or the handle to
%      the existing singleton*.
%
%      EXCEED('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EXCEED.M with the given input arguments.
%
%      EXCEED('Property','Value',...) creates a new EXCEED or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before EXCEED_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to EXCEED_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help EXCEED

% Last Modified by GUIDE v2.5 09-Sep-2016 16:27:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @EXCEED_OpeningFcn, ...
    'gui_OutputFcn',  @EXCEED_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before EXCEED is made visible.
function EXCEED_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to EXCEED (see VARARGIN)

set(handles.checkbox1, 'Value', 0);     % By default no recursive
set(handles.checkbox2, 'Value', 1);     % By default yes .ExamCard export
set(handles.checkbox3, 'Value', 1);     % By default yes rename
set(handles.checkbox4, 'Value', 1);     % By default yes .mat export
set(handles.checkbox5, 'Value', 0);     % By default no .txt export

handles.recursive = get(handles.checkbox1,'Value');
handles.export    = get(handles.checkbox2,'Value');
handles.rename    = get(handles.checkbox3,'Value');
handles.mat       = get(handles.checkbox4,'Value');
handles.txt       = get(handles.checkbox5,'Value');

% Choose default command line output for EXCEED
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes EXCEED wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = EXCEED_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
handles.startpath = get(hObject, 'String');

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
directoryname = uigetdir('','Select the starting directory');
set(handles.edit1, 'String', directoryname);
handles.startpath = directoryname;

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1
handles.recursive = get(hObject,'Value');

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2
handles.export = get(hObject,'Value');

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3
handles.rename = get(hObject,'Value');

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox4
handles.mat = get(hObject,'Value');

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in checkbox5.
function checkbox5_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox5
handles.txt = get(hObject,'Value');

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global UICONTINUE;
global RUNNING;
RUNNING = false;

if ~(handles.export || handles.rename)
    beep
    msgbox('Really? No rename & no export? REALLY?! What is my purpose in life..?', ...
        'Existential Crisis', 'warn');
    return;
end

if  (~isfield(handles,'startpath') || ~ischar(handles.startpath) || ~isdir(handles.startpath))
    beep
    msgbox('Please provide a valid starting folder', ...
        'Where to begin?', 'warn');
    return;
end

try
    logfile = ['EXCEED ' datestr(now) '.log'];
    logfile = regexprep(logfile, '[\\/:*?"<>|]','.');
    logpath = fullfile(handles.startpath,logfile);
    diary(logpath);
    disp(['EXCEED starting: ' datestr(now)])
    tic;
    
    UICONTINUE = true;
    guidata(hObject, handles); % Update handles structure
    
    set(handles.pushbutton4,'Visible','on'); drawnow;
    RUNNING = true;
    RenameAndExportExamCardFromDicom (handles.startpath, ...
        handles.rename,    ...
        handles.export,    ...
        handles.recursive, ...
        handles.mat,       ...
        handles.txt)
    set(handles.pushbutton4,'Visible','off'); drawnow;
    RUNNING = false;
    
    if UICONTINUE == true
        disp(['EXCEED succesfully finished: ' datestr(now)])
    else
        disp(['EXCEED stopped by user: ' datestr(now)])
    end
    
    toc;
    diary off
catch ME
    disp(['EXCEED encountered an error: ' datestr(now)])
    rethrow(ME)
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.figure1)


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global UICONTINUE;
UICONTINUE = false;
disp('Trying to stop...')
guidata(hObject, handles); % Update handles structure


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
global RUNNING
if isempty(RUNNING)
    RUNNING = false;
end

if RUNNING
    beep
    msgbox('EXCEED is still running! Please wait until finished or press stop...', ...
        'Not done yet!','error')
else
    delete(hObject)
end
