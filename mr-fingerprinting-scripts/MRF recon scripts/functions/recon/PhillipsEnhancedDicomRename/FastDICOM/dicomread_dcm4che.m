function [img, info] = dicomread_dcm4che(filename, rescale)

if (nargin < 2 || isempty(rescale))
    rescale = false;
end

loadDcm4che

try
    din = org.dcm4che3.io.DicomInputStream(java.io.BufferedInputStream(java.io.FileInputStream(filename)));
catch
    img = [];
    info = [];
    return
end


info = din.readDataset(-1,-1);

rows = info.getInt(org.dcm4che3.data.Tag.Rows, 0);
cols = info.getInt(org.dcm4che3.data.Tag.Columns, 0);
slices = info.getInt(org.dcm4che3.data.Tag.NumberOfFrames, 1); % For enhanced dicom

% TODO: Check if DicomImageReader is faster to use (might also be more memory efficient)
if (info.contains(org.dcm4che3.data.Tag.PixelData))
    img = permute(reshape(info.getInts(org.dcm4che3.data.Tag.PixelData), cols,rows,slices),[2 1 3]);
    info.remove(org.dcm4che3.data.Tag.PixelData);
else
    img = [];
end

din.close();

info = dicomattr(info);

if (rescale)
    if (isfield(info, 'RescaleSlope') && isfield(info, 'RescaleIntercept'))
        img = img * info.RescaleSlope + info.RescaleIntercept;
    end
end

end

