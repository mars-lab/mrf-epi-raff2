function [f] = fieldnames (seq)

f = cell(seq.info.size,1);
for I=1:seq.info.size
    f{I} = sprintf('Item_%d', I);
end


end

