function [v] = subsref(attr, S)

if (strcmp(S(1).type,'.'))
    name = S(1).subs;
    
    if (length(name) == 8 && all((name >= '0' & name <= '9') | (name >= 'a' & name <= 'f') | (name >= 'A' & name <= 'F')))
        tag = hex2dec(name);
    else
        tag = dicomlookupint(name);
        
        if (isempty(tag))
            error('Tag name not found')
        end
    end
elseif (strcmp(S(1).type,'()'))
    tag = S(1).subs;
    if (numel(tag) > 1 || numel(tag{1}) > 1)
        error('Only single index allowed for dicomattr')
    end
    tag = tag{1};
else
    error('No () or {} indexing for dicomattr allowed');
end

% This could be faster if dcm4che directly allowed access to the internal arrays using an index (obtained by indexOf(tag))
if (~attr.info.contains(tag))
    error('Tag not in attributes')
end

c = attr.info.getVR(tag).code(); % Use org.dcm4che3.data.VR.<CODE>.code to get integer value associated with VR, much faster than using vr == vr.<CODE> and allows using switch/case

switch (c)
    case 21329 % SQ
        v = dicomseq(attr.info.getSequence(tag));
    case {21843, 21331, 18771,21836,21324} % Integer types: US SS SL UL IS (note that these are returned as java int, which is not big enough to hold the max value of unsigned long (UL))
        v = attr.info.getInts(tag);
    case 17996 % Float type: FL
        v = attr.info.getFloats(tag);
    case {17491, 17988} % Double types: FD DS
        v = attr.info.getDoubles(tag);
    case {17235, 21332, 21320, 21833, 19535, 19540, 16723, 17473, 17492, 20558, 21844, 21581} % String types: CS ST SH UI LO LT AS DA DT PN UT TM
        v = char(attr.info.getBytes(tag))'; % Faster than dcm4che's getStrings
        endInd = find(v==0);
        if (~isempty(endInd))
            v = v(1:endInd-1);
        end
        v = deblank(v);
    otherwise
        v = attr.info.getBytes(tag);
end

if (length(S)>1)
    v = subsref(v, S(2:end));
end

end

