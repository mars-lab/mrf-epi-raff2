function loadDcm4che

try
    % See if class from core dcm4che exists
    org.dcm4che3.util.TagUtils;
    %     error('Not a real error') % REMOVE THIS LINE!!!
catch
    libpath = fullfile(fileparts(mfilename('fullpath')), 'dcm4che-3.3.7' , 'lib');
    javaaddpath(fullfile(libpath, 'dcm4che-core-3.3.7.jar'));
    javaaddpath(fullfile(libpath, 'dcm4che-image-3.3.7.jar'));
    javaaddpath(fullfile(libpath, 'dcm4che-imageio-3.3.7.jar'));
    javaaddpath(fullfile(libpath, 'slf4j-api-1.7.5.jar'));
    javaaddpath(fullfile(libpath, 'slf4j-log4j12-1.7.5.jar'));
    javaaddpath(fullfile(libpath, 'log4j-1.2.17.jar'));
end

end
