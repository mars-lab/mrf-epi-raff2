function files=scanInfo(MRFdir)
%% Function that genererates a struct of all dicom files in folder and extracts all important MRF info out
% Author: Telly Ploem
% Please read in the function how to modify the code so it will work for
% your custom parameters you need to do this!!
% The function also requires a modified version of EXCEED which is
% contained in the same folder


dcm_files=dir(strcat(MRFdir,'/*.dcm'));

dcm_files = dcm_files(~startsWith({dcm_files.name}, '.'));
%% try to look for scan paramter files with same name
mat_files=dir([MRFdir '/*.mat']);

for i=1:length(dcm_files)
    dcm_name=dcm_files(i).name;
    pattern=extractBefore(dcm_name,'.');
    for j=1:length(mat_files)
        mat_name=mat_files(j).name;
        if startsWith(mat_name,pattern)
            dcm_files(i).mat_name=mat_name;
        end
    end

end

%% now load all the mat files and add the info


for i=1:length(dcm_files)
    try
        baseFileName = dcm_files(i).mat_name;
        fullFileName = fullfile(dcm_files(i).folder, baseFileName);
        load(fullFileName);


        % fill with standard XX info (FOV, N_dyn, resolution) (feel free to add
        % your own)
        dcm_files(i).XXinfo=XXinfo; % Also save the full XXinfo





        image_seq_enum=["MGUACQ_SEQ_SE", "MGUACQ_SEQ_IR", "MGUACQ_SEQ_MIXED", "MGUACQ_SEQ_FFE", "MGUACQ_SEQ_ECHO", "MGUACQ_SEQ_FID"];
        dcm_files(i).image_seq=image_seq_enum(XXinfo.EX_ACQ_imaging_sequence+1);
        fast_image_enum=["(MGUACQ_FAST_NO)",       "(MGUACQ_FAST_TSE)",   "(MGUACQ_FAST_EPI)",  " (MGUACQ_FAST_GRASE)"   , "(MGUACQ_FAST_TFE_EPI)" ,"(MGUACQ_FAST_TSI)"];
        dcm_files(i).Fast_imaging_mode=fast_image_enum{XXinfo.EX_ACQ_fast_imaging_mode+1};

        dcm_files(i).voxel_size=XXinfo.EX_GEO_voxel_size_m;
        dcm_files(i).EX_GEO_fov=XXinfo.EX_GEO_fov;
        dcm_files(i).NR_dyn=XXinfo.EX_DYN_nr_scans;
        dcm_files(i).slice_thickness=XXinfo.EX_GEO_acq_slice_thickness;
        dcm_files(i).N_slice=max(XXinfo.EX_GEO_stacks_slices);
        %dcm_files(i).TR=XXinfo.IF_act_FFE_rep_time;
        %dcm_files(i).TE=XXinfo.EX_ACQ_echo_time_step;
        dcm_files(i).TR_and_TE=XXinfo.IF_act_rep_time_echo_time;
        dcm_files(i).TR_and_TE=string(dcm_files(i).TR_and_TE{1});
        dcm_files(i).TE=str2double(extractAfter(string(dcm_files(i).TR_and_TE),'/'));
        dcm_files(i).TR=str2double(extractBefore(string(dcm_files(i).TR_and_TE),'/'));
        RF_shim=XXinfo.EX_ACQ_rf_shim;%XXinfo.EX_ACQ_RF_shim;
        dcm_files(i).RF_shim=RF_shim;
        ori=XXinfo.EX_GEO_cur_stack_orientation;
        switch ori
            case 0
                orientation='TRANSVERSAL';
            case 1
                orientation='SAGITTAL';
            case 2
                orientation='CORONAL';
            otherwise
                orientation='unknown'
        end
        dcm_files(i).orientation=orientation;

        dcm_files(i).slice_gap=XXinfo.IF_act_slice_gap;
        dcm_files(i).meas_voxel_size=XXinfo.IF_meas_voxel_size;
        dcm_files(i).recon_voxel_size=XXinfo.IF_recon_voxel_size;
        dcm_files(i).meas_matrix_size=XXinfo.IF_meas_matrix_size;
        %dcm_files(i).half_scan_factor=XXinfo.IF_act_half_scan_factor;
        dcm_files(i).sense_enable=XXinfo.EX_GEO_sense_enable;
        if XXinfo.EX_GEO_sense_enable==1
            dcm_files(i).sense_factor=max(max(XXinfo.EX_GEO_sense_m_red_factor,XXinfo.EX_GEO_sense_p_red_factor),XXinfo.EX_GEO_sense_s_red_factor);
        else
            dcm_files(i).sense_factor=1;
        end



        % add custom parameters that you want for example: (you NEED to change this
        % as you probably don't have these parameters
        dcm_files(i).MRF=XXinfo.EX_ACQ_MRF_enable;
        dcm_files(i).Inversion=XXinfo.EX_ACQ_MRF_inversion;
        dcm_files(i).Inv_HS=XXinfo.EX_ACQ_MRF_inversion_HS;
        dcm_files(i).Inv_extra=XXinfo.EX_ACQ_MRF_MS_extrainversion;


        dcm_files(i).MRF_IR_TSE=XXinfo.EX_ACQ_MRF_IR_TSE;
        %% REPP info
        dcm_files(i).REPP=XXinfo.EX_REPP_enable;
        dcm_files(i).T1rho_repp=XXinfo.EX_REPP_spinlock;
        dcm_files(i).RAFF_enable=XXinfo.EX_REPP_RAFF_pulse;
        dcm_files(i).SL_time=XXinfo.EX_REPP_spinlock_dur;
        dcm_files(i).SL_freq=XXinfo.EX_REPP_spinlock_freq;
        dcm_files(i).SL_type=XXinfo.EX_REPP_spinlock_type;
        dcm_files(i).T1rho_mapping=XXinfo.EX_REPP_T1rho_mapping;
        dcm_files(i).RAFF_freq=XXinfo.EX_REPP_RAFF_freq;
        dcm_files(i).RAFF_blips=XXinfo.EX_REPP_RAFF_blips;
        try
            dcm_files(i).RAFF_bl_str=XXinfo.EX_REPP_RAFF_bl_str;
        catch
        end
        try
            dcm_files(i).RAFF_rand_bl_or=not(not(XXinfo.EX_REPP_RAFF_rand_bl_or));
        catch
        end
        try
            if  dcm_files(i).RAFF_rand_bl_or==1
                dcm_files(i).RAFF_blip_ori=-1;
            else
                dcm_files(i).RAFF_blip_ori=XXinfo.EX_REPP_RAFF_blip_ori;
            end
        catch
        end
        dcm_files(i).RAFF_blip_dur=XXinfo.EX_REPP_RAF_blip_dur;
        try
            dcm_files(i).RAFF_spoil_3_axis=XXinfo.EX_REPP_RAFF_spoil_3_ax;
        catch
        end
        try
            if XXinfo.EX_REPP_RAFF_cu_sp_dur==1
                dcm_files(i).RAFF_spoil_dur=XXinfo.EX_REPP_RAFF_spoil_dur;
            else
                dcm_files(i).RAFF_spoil_dur=-1;
            end
        catch
        end
        %% MRF REPP
        dcm_files(i).MRF_REPP=XXinfo.EX_ACQ_MRF_REPP;
        dcm_files(i).MRF_REPP_jump=XXinfo.EX_ACQ_MRF_T1r_M2D_jump;
        dcm_files(i).MRF_REPP_extra_REPP_fact=XXinfo.EX_ACQ_MRF_extra_REPP_fact;
        dcm_files(i).MRF_REPP_in_2nd=XXinfo.EX_ACQ_MRF_REPP_in_2nd;
        dcm_files(i).MRF_REPP_train_txt=XXinfo.EX_ACQ_MRF_T1r_train_txt;
        dcm_files(i).MRF_REPP_pause_dur=XXinfo.EX_ACQ_MRF_T1rho_pause_dur;
        %% Save SL or RAFF blocks
        SL_times=XXinfo.EX_MDME_MRF_SLt_save;
        SL_times([SL_times==-1])=[];

        dcm_files(i).SL_times=SL_times;

        RAFF_blocks=XXinfo.EX_MDME_MRF_RAFF_save;
        RAFF_blocks([RAFF_blocks==-1])=[];
        dcm_files(i).RAFF_blocks=double(RAFF_blocks);
        FA_save=XXinfo.EX_MDME_MRF_fa_save;
        TE_save=XXinfo.EX_MDME_MRF_te_save;
        FA_save([FA_save==-1])=[];
        TE_save([TE_save==-1])=[];
        dcm_files(i).FA_train=FA_save;
        dcm_files(i).TE_train=TE_save;


    catch
    end
end

files=dcm_files;
try
    save(strcat(MRFdir, '/Scan_overview'),'files');
catch
end
try
    writetable(struct2table(files), strcat(MRFdir, '/Scan_overview','.xlsx'))
    disp(['succesfully saved scan overview as' strcat(MRFdir, '/Scan_overview','.xlsx')])
catch
end
end