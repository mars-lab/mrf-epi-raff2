function [dict_input]=create_dict_input(increment,XXinfo,MRF_dicom_info)
N_slice = double(max(MRF_dicom_info.dyn_slice(2,:)));

N_dyn = MRF_dicom_info.NumberDyamics;

try 
    dict_input.shuffle=XXinfo.EX_ACQ_MRF_REPP_shuf_RAFF;
    dict_input.new_patch=1;
catch
     dict_input.new_patch=0;
end

FA = double(XXinfo.EX_MDME_MRF_fa_save);
FA = FA(1:N_dyn);
FA(FA == -1) = [];
TE = double(XXinfo.EX_MDME_MRF_te_save);
TE = TE(1:N_dyn);
TE(TE == -1) = [];
extra_inversion = XXinfo.EX_ACQ_MRF_MS_extrainversion;

try
    break_MRF = XXinfo.CSC_MRF_break_before_acq;
catch
    break_MRF = 0; %in  older patches we didnt have a break
end

TR_base = MRF_dicom_info.TR;
TE_base = MRF_dicom_info.TE;
TR = TR_base + TE;
TE = TE + TE_base;

MRF_repp=XXinfo.EX_ACQ_MRF_REPP;
RefDic=createREFDic(MRF_repp,increment);
RFpulses = FA/180*pi;
Slice_Order=interleavedSliceOrder(N_slice);
if XXinfo.EX_REPP_RAFF_pulse==1
Tp=(2*1000)/(sqrt(2)*XXinfo.EX_REPP_RAFF_freq);
else
    Tp=1;
end
REPP_times=[];
if XXinfo.EX_REPP_RAFF_pulse==1
REPP_times=double(XXinfo.EX_MDME_MRF_RAFF_save);
REPP_times(REPP_times==-1)=[];
REPP_times=REPP_times*Tp;
end
if XXinfo.EX_REPP_spinlock==1
REPP_times=double(XXinfo.EX_MDME_MRF_SLt_save(XXinfo.EX_MDME_MRF_SLt_save>-1));
if length(REPP_times)==9 
    if REPP_times(9)==60
    REPP_times(end+1)=68;
    end
    if REPP_times(9)==36
    REPP_times(end+1)=40;
    end
end
end
SL_wait=XXinfo.EX_ACQ_MRF_T1rho_pause_dur;
M2D_jump=double(XXinfo.EX_ACQ_MRF_T1r_M2D_jump);
try
Extra_repp_fact=double(XXinfo.EX_ACQ_MRF_extra_REPP_fact);
catch
    Extra_repp_fact=1;
end
try
    REPP_in_2nd=XXinfo.EX_ACQ_MRF_REPP_in_2nd;
catch
    REPP_in_2nd=0;
end
%% dict input
dict_input.RFpulses=RFpulses;
dict_input.RefDic=RefDic;
dict_input.N_slice=N_slice;
dict_input.TR=TR;
dict_input.TE=TE;
dict_input.Slice_Order=Slice_Order;
dict_input.extra_inversion=extra_inversion;
dict_input.break_MRF=1;
dict_input.REPP_times=REPP_times;

dict_input.N_no_SL =double(XXinfo.EX_MDME_MRF_Ndef_train__save);
dict_input.SL_wait= SL_wait;
dict_input.M2D_jump =M2D_jump;
dict_input.extra_repp_factor =Extra_repp_fact;
dict_input.REPP_in_2nd=REPP_in_2nd;
dict_input.XXinfo=XXinfo;
end
