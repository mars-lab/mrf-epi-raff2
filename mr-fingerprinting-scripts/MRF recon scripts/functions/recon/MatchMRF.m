function [MaxCorrVal, CorrelationCoeff] = MatchMRF(D, MRFBilder)

sz = 0;

if length(size(MRFBilder)) == 2
    [a1,a2] = size(MRFBilder);
    M = MRFBilder;
    sz = 2;
elseif length(size(MRFBilder)) == 3
    [a1,a2,a3] = size(MRFBilder);
    M = reshape(MRFBilder, a1*a2, a3);
    sz = 3;
else
    disp('Wrong dimensions')
end

[dd1,dd2] = size(M);

D = single(abs(D));
D = D./repmat(sqrt(sum(D.^2,2)), 1,dd2);

M = single(abs(M));
M = M./repmat(sqrt(sum(M.^2,2)), 1, dd2);

% calculate max memory (a bit of a guess, but better than nothing)
CorrelationCoeff = zeros(dd1,1);
MaxCorrVal = ones(dd1,1);

if ispc
    mem = memory;
    N = round(mem.MaxPossibleArrayBytes./(2*4*length(D)*1.7)/2);
    N_loop = ceil(dd1/N);
    for jloop = 1:ceil(dd1/N)
        idx = (jloop-1)*N+1:min(jloop*N, dd1);
        [CorrelationCoeff(idx), MaxCorrVal(idx)] = max(D*M(idx,:)');
    end
end

if isunix
    N = 500;
    N_loop = ceil(dd1/N);
    for jloop = 1:ceil(dd1/N)
%         disp(num2str(jloop/N_loop));
        idx = (jloop-1)*N+1:min(jloop*N, dd1);
        
        [CorrelationCoeff(idx), MaxCorrVal(idx)] = max(D*M(idx,:)');
    end
end

if sz == 3
    CorrelationCoeff = reshape(CorrelationCoeff, a1, a2);
    MaxCorrVal = reshape(MaxCorrVal, a1, a2);
end