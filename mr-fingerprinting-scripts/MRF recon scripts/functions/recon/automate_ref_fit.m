tic()
MRFdir=uigetdir('open input folder containning the DICOMS');
OutDir=uigetdir(MRFdir,'specify output dir');
mkdir(OutDir);
disp('Start renaming files')
RenameAndExportExamCardFromDicom (MRFdir, 1, 1, 0, 1, 0) %non recursive folder search you can change this
files=scanInfo(MRFdir);

for i=1:length(files)
    try
        filename=fullfile(files(i).folder,files(i).name);
        XXinfo=files(i).XXinfo;
        
        if XXinfo.EX_REPP_RAFF_mapping==1
            image=loadPhillipsDicom(filename);
            FitResults=RAFF_fit(image,XXinfo);
            
            [filepath,name,ext] = fileparts(XXinfo.Filename);
            figure;
            showMap(FitResults.T2); caxis([ 000 200]);
            title('T_{RAFF} map'); axis off
            saveFig(['TRAFFref_',name]);
            close figure
            
            save([OutDir,'/RAFF_FitResults_',name],'FitResults');
        end
        if XXinfo.EX_ACQ_MRF_IR_TFE==1
            image=loadPhillipsDicom(filename);
            FitResults=IR_TFE_fit(image,XXinfo);
            showMap(FitResults.T1); caxis([ 000 3000]);
            title('T_1 map'); axis off
            saveFig(['T1ref_',name]);
        end
    catch
    end
end
toc()