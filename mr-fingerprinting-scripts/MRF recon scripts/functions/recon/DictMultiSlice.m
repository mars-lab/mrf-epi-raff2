function [dict] = DictMultiSlice(RFpulses, RefDic ,TR , TE,N_slice,slice_order,extra_inversion,pre_scan)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

RefDic(:,RefDic(1,:) < RefDic(2,:)) =  [];

T1 = RefDic(1,:);
T2star = RefDic(2,:);
AngleCorrection = RefDic(4,:);

if size(RFpulses,2)~=size(AngleCorrection,1)
    RFpulses=RFpulses';
end

rf = abs(RFpulses*AngleCorrection);  % acount for angle correction
T_spir = 11;
d1 = size(RefDic,2);         % number of fingerprints
d2 = length(RFpulses);       % length of fingerprints
dict = zeros(d1,d2,N_slice); % output dictionary
N_dyn = d2;
M0 = zeros(3,d1,N_slice);    % temporarz magnetization vector, [mx,my,mz] (3*N_dyn*N_slice)
M0(3,:,:) = 1;

for dyn = 1:N_dyn
    for loc = 1:N_slice
        slice = slice_order(loc);
        if (dyn == 1 && loc == 1)
            % Prescan on first slice if no wait
            if pre_scan == 1
                %Flip
                M0(:,:,slice) = rx_flip3D(M0(:,:,slice) , rf(1,:) );
                
                % relax by TE
                M0(:,:,slice) = relx3D(M0(:,:,slice) , TE(1), T1, T2star);
                
                %no readout
                % relax
                M0(:,:,slice) = relx3D(M0(:,:,slice) , TR(1) - TE(1), T1, T2star);
                
                % spoil transversal magnatization
                M0(1:2,:,slice) = 0;
            end
            
            % invert all slices
            for k = 1:size(M0,3)
                M0(:,:,k) = rx_flip3D(M0(:,:,k), pi);
                M0(:,:,k) = relx3D(M0(:,:,k), 12, T1, T2star);
                M0(1:2,:,k) = 0;
            end
            
            % Acquire current slice and relax others
            M0(:,:,slice) = relx3D(M0(:,:,slice) ,T_spir, T1, T2star);% relax by SPIR time
            
            % Flip
            M0(:,:,slice) = rx_flip3D(M0(:,:,slice) , rf(1,:));
            M0(:,:,slice) = relx3D(M0(:,:,slice) , TE(dyn), T1, T2star); % relax by TE
            
            dict(:,dyn,slice)=(M0(1,:,slice)+1i.*M0(2,:,slice))'; % readout
            
            M0(:,:,slice) = relx3D(M0(:,:,slice) , TR(dyn) - TE(dyn), T1, T2star); % relax
            M0(1:2,:,slice) = 0; % spoil transversal magnatization
            
            for k = setdiff(1:N_slice,slice)
                M0(:,:,k) = relx3D(M0(:,:,k) , TR(dyn), T1, T2star);
            end % relax all others
        end
        
        if (dyn==1 && loc~=1)
            % Flip
            M0(:,:,slice) = rx_flip3D(M0(:,:,slice) , rf(dyn,:));
            M0(:,:,slice) = relx3D(M0(:,:,slice) , TE(dyn), T1, T2star); % relax by TE
            
            M0(:,:,slice) = relx3D(M0(:,:,slice) , TR(dyn) - TE(dyn), T1, T2star); % relax
            M0(1:2,:,slice) = 0; % spoil transversal magnatization
            
            for k=setdiff(1:N_slice,slice)
                M0(:,:,k) = relx3D(M0(:,:,k) , TR(dyn), T1, T2star);
            end
            
            % Acquire current slice and relax others
            % Flip
            M0(:,:,slice) = rx_flip3D(M0(:,:,slice) , rf(1,:));
            M0(:,:,slice) = relx3D(M0(:,:,slice) , TE(dyn), T1, T2star); % relax by TE
            
            dict(:,dyn,slice)=(M0(1,:,slice)+1i.*M0(2,:,slice))'; % readout
            
            M0(:,:,slice) = relx3D(M0(:,:,slice) , TR(dyn) - TE(dyn), T1, T2star); % relax
            M0(1:2,:,slice) = 0; % spoil transversal magnetization
            
            for k = setdiff(1:N_slice,slice)
                M0(:,:,k) = relx3D(M0(:,:,k) , TR(dyn), T1, T2star);
            end
        end
        
        if dyn ~= 1
            if extra_inversion == 1 && mod((dyn-1),2) == 0 && ((dyn-1)*2 == (loc-1)) %% invert if conditions are met
                disp(strcat('Extra inversion pulse in dyn = ',num2str(dyn), ', loc = ', num2str(loc), ', slice = ', num2str(slice)))
                % invert all slices
                for k = 1:size(M0,3)
                    M0(:,:,k) = rx_flip3D(M0(:,:,k), pi);
                    M0(:,:,k) = relx3D(M0(:,:,k), T_spir, T1, T2star);
                    M0(1:2,:,k) = 0;
                end
            end
            % Acquire current slice and relax others
            % Flip
            M0(:,:,slice) = rx_flip3D(M0(:,:,slice) , rf(dyn,:));
            M0(:,:,slice) = relx3D(M0(:,:,slice) , TE(dyn), T1, T2star); % relax by TE
            
            dict(:,dyn,slice)=(M0(1,:,slice)+1i.*M0(2,:,slice))'; % readout
            
            M0(:,:,slice) = relx3D(M0(:,:,slice) , TR(dyn) - TE(dyn), T1, T2star); % relax
            M0(1:2,:,slice) = 0; % spoil transversal magnatization
            
            for k = setdiff(1:N_slice,slice)
                M0(:,:,k) = relx3D(M0(:,:,k) , TR(dyn), T1, T2star);
            end
        end
    end
end
end