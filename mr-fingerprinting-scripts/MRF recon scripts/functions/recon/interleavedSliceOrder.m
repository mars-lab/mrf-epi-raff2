function [sliceOrder] = interleavedSliceOrder(N_slice)
%INTERLEAVEDSLICEORDER Summary of this function goes here
%   Detailed explanation goes here
interleav_N = round(sqrt(N_slice));
ind = 0;
j = 0;

for i = 0:N_slice-1
    slice_num = interleav_N*j+ind;
    
    if slice_num > N_slice-1
        j = 0;
        ind = ind+1;
        slice_num = interleav_N*j+ind;
    end
    
    sliceOrder(i+1) = slice_num;
    j = j+1;
end

sliceOrder = sliceOrder+1;
end

