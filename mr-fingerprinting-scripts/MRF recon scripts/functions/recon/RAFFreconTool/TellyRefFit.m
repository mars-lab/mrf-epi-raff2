tic()
MRFdir=uigetdir('open input folder containning the DICOMS');
OutDir=uigetdir(MRFdir,'specify output dir');
% mkdir(OutDir);
disp('Start renaming files')
RenameAndExportExamCardFromDicom (MRFdir, 1, 1, 0, 1, 0) %non recursive folder search you can change this
files=scanInfo(MRFdir);

for i=1:length(files)
    try
        filename=fullfile(files(i).folder,files(i).name);
        XXinfo=files(i).XXinfo;
        
        if XXinfo.EX_REPP_RAFF_mapping==1
            image=loadPhillipsDicom(filename);
            FitResults=RAFF_fit(image,XXinfo);
            
            [filepath,name,ext] = fileparts(XXinfo.Filename);
            figure;
            showMap(FitResults.T2); caxis([ 000 200]);
            title('T_{RAFF} map'); axis off
            saveFig([OutDir,'/TRAFFref_',name]);
            close figure
            
            save([OutDir,'/RAFF_FitResults_',name],'FitResults');
        end
        if XXinfo.EX_ACQ_MRF_IR_TFE==1
            image=loadPhillipsDicom(filename);
            FitResults=IR_TFE_fit(image,XXinfo);
            figure;
            showMap(FitResults.T1); caxis([ 000 3000]);
            title('T_1 map'); axis off
            saveFig([OutDir,'/T1ref_',name]);
            close figure
            save([out_dir,'/T1_FitResults_',name],'FitResults');
        end
    catch
    end
end
toc()

function saveFig(output_name)
saveas(gcf,[output_name]);
exportgraphics(gcf,[output_name,'.png'],'Resolution',700)
saveas(gcf,[output_name,'.svg'])
end

function  im=showMap(map)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
im=imagesc(abs(squeeze(map))); axis equal tight; colormap parula; colorbar;
end

