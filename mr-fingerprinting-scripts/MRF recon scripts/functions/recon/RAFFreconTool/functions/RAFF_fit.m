function FitResults=RAFF_fit(image,XXinfo)
RAFF_blocks=XXinfo.EX_MDME_MRF_RAFF_save;
RAFF_blocks=double(RAFF_blocks(RAFF_blocks~=-1));
Tp=(2*1000)/(sqrt(2)*double(XXinfo.EX_REPP_RAFF_freq));
RAFF_times=RAFF_blocks.*Tp;

magn_data=image(:,:,1:length(RAFF_times));
magn_data=magn_data./max(magn_data,[],3);
fit_im=zeros(size(magn_data,1),size(magn_data,2),1,size(magn_data,3));
fit_im(:,:,1,:)=magn_data;
mask=ones(size(magn_data,1),size(magn_data,2));

data = struct();
Model = mono_t2;
Model.Prot.SEdata.Mat = [ RAFF_times ];
Model.options.OffsetTerm=0;

data.SEdata=fit_im;

data.Mask=mask;
gcp;
FitResults = ParFitData(data,Model);
FitResults.XXinfo=XXinfo;


end


