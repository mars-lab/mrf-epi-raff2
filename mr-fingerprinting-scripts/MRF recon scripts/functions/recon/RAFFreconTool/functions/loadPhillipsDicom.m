function [data_rescaled, dicom_info] = loadPhillipsDicom(filename)
%% Loads and rescales the phillips dicom data

dicomdict('set','dicom-dict-philips.txt')
inf = dicominfo(filename);
im = dicomread(inf);
frame_in = inf.PerFrameFunctionalGroupsSequence;
fn = fieldnames(frame_in);

for k=1:length(fn)
    a(k)=frame_in.(fn{k});
end

%% fast rescale
for i = 1:length(a)
    rescale_intercept(i)    = [a(i).PrivatePerFrameSq.Item_1.RescaleIntercept];
    rescale_slope(i)        = [a(i).PrivatePerFrameSq.Item_1.RescaleSlope];
    frame_acq{i}            = (a(i).FrameContentSequence.Item_1.FrameAcquisitionDatetime);
    DimensionIndexValues{i} = a(i).FrameContentSequence.Item_1.DimensionIndexValues;
end

%% slice and dyn
for i = 1:length(a)
    slice(i)   = DimensionIndexValues{1,i}(2);
    try
    dyn(i)     = DimensionIndexValues{1,i}(3);
    catch
    end
    timings{i} = datetime(frame_acq{i},'InputFormat','yyyyMMddHHmmss.SS');
end

%% rescale
frames = 1:length(a);
slice_dyn_combined = [frames; slice; dyn;];

im = single(squeeze(im));
data_rescaled = zeros(size(im));
for i = 1:size(im,3)
    data_rescaled(:,:,i) = single((im(:,:,i).*rescale_slope(i)) + rescale_intercept(i));
end

dicom_info.TE              = str2double(inf.MRSeriesEchoTimeDisplay);
dicom_info.TR              = inf.MRSeriesRepetitionTime(1);
dicom_info.fat_sur         = inf.MRFatSaturationTechnique;
dicom_info.Width           = inf.Width;
dicom_info.Height          = inf.Height;
dicom_info.NumberOfFrames  = inf.NumberOfFrames;
dicom_info.NumberDyamics   = inf.MRSeriesNrOfDynamicScans;
dicom_info.Filename        = inf.Filename;
dicom_info.Date            = inf.PerformedProcedureStepStartDate;
dicom_info.dyn_slice       = slice_dyn_combined;
dicom_info.full_dicom_info = inf;
end