function xxinfo = XXread(file_name)
% Read Philips XX files (DICOM Raw Data Storage containing exam parameters)
%  Useful for finding exam and info-page parameters that were present
%  during scanning.
%
%  xxinfo = XXread(file_name) - read XX file_name and output to a structure
%  xxinfo = XXread            - calls UI for input file name,
%           XXread( ... )     - with no output specified, it writes a text
%                               file to 'file_name.txt'
%
%  xxinfo is a structure composed of the parameters decoded from the XX
%  file.
%
%  Note all parameters are output and may have default values that
%  are meaningless for the specific scan. It is not clear when the
%  parameter values are taken (before or after scan?).
%
%
% The XX file is a DICOM format file output from Philips MR scanners. It is
% not always transmitted through PACs systems. There is one XX file per
% series and often additional XX files probably related to the ExamCard as
% a whole.
% These files are 'RAW' with SOPClassUID '1.2.840.10008.5.1.4.1.1.66'
%
% THIS SOFTWARE IS PROVIDED  "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
%
% Requires dicomread in Image Processing Toolbox.
%
% COILSTATE and HARDWARE_CONFIG present in R5.1.7 XX files are not
% currently extracted.
%
% See https://github.com/malaterre/GDCM/blob/master/Applications/Cxx/gdcmdump.cxx
% Process SDS data
%
% Cannot as yet decode the ExamCard XX files. For possible clues, see:
% https://github.com/IBIC/ibicUtils/blob/master/ExamCard.py
% https://irc.cchmc.org/software/libexamcard/doc/examcard2xml_8c-example.html
%
% David Atkinson  D.Atkinson@ucl.ac.uk (original version, as posted on the
% Philips forum:
% https://community.mr-paradise.com/t/extract-parameters-from-xx--dicom-export-files/513/2?u=mike )
% Mike van Rijssel  mrijsse2@umcutrecht.nl (edit on 07 - 09 - 2016)
%
% See also DICOMINFO

% From https://github.com/malaterre/GDCM/blob/master/Applications/Cxx/gdcmdump.cxx
%
% static void ProcessSDSData( std::istream & is )
% {
%   // havent been able to figure out what was the begin meant for
%   is.seekg( 0x20 - 8 );
%   uint32_t version = 0;
%   is.read( (char*)&version, sizeof(version) );
%   assert( version == 8 );
%   uint32_t numel = 0;
%   is.read( (char*)&numel, sizeof(numel) );
%   for( uint32_t el = 0; el < numel; ++el )
%     {
%     PDFElement pdfel;
%     assert( sizeof(pdfel) == 50 );
%     is.read( (char*)&pdfel, 50 );
%     if( *pdfel.getname() )
%       {
%       printbinary( is, pdfel );
%       std::cout << std::endl;
%       }
%     }
%
% }

verb = false ; % VERBOSE, set true for debugging/screen output.

if nargout > 0
    sout = true ; % structure output
    fout = false; % file out
else
    sout = false ;
    fout = true; % file out
end


% Determine XX file name. Use pref_uigetfile if present, otherwise native
% MATLAB uigetfile. Quit if user cancels UI.
if nargin <1 || ~exist(file_name,'file')
    if ~exist('pref_uigetfile','file')
        [fxx,pxx,~] = uigetfile('*.*','Enter the XX file') ;
        if isequal(fxx,0)
            xxinfo = [] ;
            return
        else
            file_name = fullfile(pxx, fxx) ;
        end
    else
        file_name = pref_uigetfile('XXread','filename') ;
        if isempty(file_name)
            xxinfo = [] ;
            return
        end
    end
    
end
if sout ; xxinfo.Filename = file_name; end

[p,n,~] = fileparts(file_name) ;
% deffn = fullfile(p,[n,'.txt']) ;

if fout
    %     [fnout,pnout] = uiputfile('*.txt','Filename for output',deffn) ;
    f_temp = fullfile(p,[n,'.txt']);
    [pnout,nnout,enout] = fileparts(f_temp);
    fnout = [nnout,enout];
    if isequal(fnout,0) || isequal(pnout,0)
        warning('No output text file selected.')
        fout = false ;
    end
    fid = fopen(fullfile(pnout,fnout), 'wt+') ;
end

% Reset Dicom dictionary to default so as to preserve the Private tag names
% which could be overwritten if user has a modified dicom dictionary.
% Read Private tags using dicominfo
% Set dicom dictionary back

dict = dicomdict('get') ;
dicomdict('factory');
dinfo = dicominfo(file_name) ;
% dinfo = dicominfo_dcm4che(file_name);
dicomdict('set',dict) ;

if isfield(dinfo,'Private_2005_1132')  % (2005,1132) MRBlobDataObjectArray
    currpf = dinfo.Private_2005_1132 ;
    
    fnb = fieldnames(currpf);
    for iitem = 1:length(fnb)
        try
            crtyp = currpf.(fnb{iitem}).Private_2005_1139 ;  % (2005,1139) MRTypeName
            crtit = currpf.(fnb{iitem}).Private_2005_1137 ;  % (2005,1137) MRBlobName
            if isstruct(crtyp) % old style?
                crtyp = crtyp.FamilyName ;
                crtit = crtit.FamilyName ;
            end
            if strcmp(crtit,'COILSTATE'); continue; end; % skip this item if it is the COILSTATE info - not yet readable
            if strcmp(crtit,'HARDWARE_CONFIG'); continue; end; % skip this item if it is the HARDWARE_CONFIG info  - not yet readable
            
            if isa(crtyp,'uint8') % A Dicom transfer issue has probably
                % switched type to uint8
                crtyp = char(crtyp') ;
                crtit = char(crtit') ;
            end
            if strcmp(crtit,'ExamCardBlob')
                disp('ExamCardBlob')
                continue
            end
            
            switch crtyp
                case 'BINARY'
                    disp(['BINARY - not decoded: ',fnb{iitem}])
                case 'ASCII'
                    disp(['ASCII - not decoded: ',fnb{iitem}])
                otherwise
                    if verb; disp(['type: ',crtyp]); end
                    
                    cs = currpf.(fnb{iitem}).Private_2005_1144 ;  % (2005,1144)	MRBlobData
                    if (isa(cs,'uint8') || isa(cs,'int8'))
                        cs = typecast(cs,'uint16') ;
                    end
                    n = typecast(uint16(cs(15:16)),'uint32') ;
                    % str = [currpf.(fnb{iitem}).Private_2005_1137,' n = ',num2str(n)] ;
                    str = [crtit,' n = ',num2str(n)] ;
                    if verb ; disp(str) ; end
                    if fout ; fprintf(fid,'%s\n',str) ; end
                    i8all = typecast(uint16(cs),'uint8') ;
                    
                    pos = 32 ; % bytes
                    
                    for ient = 1:n
                        i8 = i8all(pos+1:pos+50) ;
                        loc = find(i8==0) ;
                        str = char(i8(1:loc(1)-1))'; % DA inserted the -1 to prevent 0 in str
                        % disp([str,' ',num2str(i8(loc(1)+1:end)')])
                        
                        typ = typecast(uint8(i8(35:38)),'uint32') ;
                        numelems = typecast(uint8(i8(39:42)),'uint32') ;
                        offset   = typecast(uint8(i8(47:50)),'uint32') ;
                        jump = pos+50+offset-4 ;
                        
                        if verb ; disp([str,' # ',num2str(numelems)]) ; end
                        if fout ; fprintf(fid,'%s\n',[str,' # ',num2str(numelems)]); end
                        if sout; fieldnm = str ; end
                        
                        switch typ
                            case 0
                                typs = 'single' ;
                                vals = typecast(uint8(i8all(jump+1:jump+numelems*4)),typs);
                                nv = length(vals) ;
                                if verb ; disp(num2str(vals(1:min(30,nv))')); end
                                if fout ; fprintf(fid,'%s\n',num2str(vals')); end
                                if sout; fieldval = vals ; end
                                
                            case 1
                                typs = 'int32' ;
                                vals = typecast(uint8(i8all(jump+1:jump+numelems*4)),typs);
                                nv = length(vals) ;
                                if verb; disp(int2str(vals(1:min(30,nv))')) ; end
                                if fout; fprintf(fid,'%s\n',int2str(vals')); end
                                if sout; fieldval = vals ;end
                                
                            case 2
                                typs = 'STR80' ;
                                vals81 = typecast(uint8(i8all(jump+1:jump+numelems*81)),'uint8');
                                vals = []; fieldval = [] ;
                                for istr = 1:numelems
                                    instrp = (istr-1)*81 ;
                                    valsstr = char(vals81(instrp+1:instrp+80))' ;
                                    if verb; disp(valsstr); end
                                    if fout; fprintf(fid,'%s\n',valsstr); end
                                    if sout; fieldval{istr} = valsstr ; end
                                end
                                
                            case 4
                                typs = 'uint32' ;
                                vals = typecast(uint8(i8all(jump+1:jump+numelems*4)),typs);
                                nv = length(vals) ;
                                if verb; disp(num2str(vals(1:min(30,nv))')) ; end
                                if fout; fprintf(fid,'%s\n',num2str(vals')); end
                                if sout ; fieldval = vals ; end
                            otherwise
                                disp(['Unknown type: ',typ])
                        end
                        if sout
                            xxinfo.(fieldnm) = fieldval ;
                        end
                        %disp([str,' # ',num2str(numelems),' type: ',num2str(typs),' offset: ',num2str(offset)])
                        %nv = length(vals) ;
                        %disp(vals(1:min(30,nv))')
                        pos = pos + 50 ;
                    end
                    if verb; disp(' '); end
                    if fout; fprintf(fid,'%s\n',' '); end
            end
            
        end
        % else
    end
    %     warning('No Private_2005_1132 found')
end


if fout
    fclose(fid);
    disp(['Saving scan parameters to: ',fullfile(pnout,fnout)])
end

