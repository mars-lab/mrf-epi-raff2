function [tag] = dicomlookupint (name)

[a,b] = dicomlookup(name);
tag = a*2^16 + b;

end

