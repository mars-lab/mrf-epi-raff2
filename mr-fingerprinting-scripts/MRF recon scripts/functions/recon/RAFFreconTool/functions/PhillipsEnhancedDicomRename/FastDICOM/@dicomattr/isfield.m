function [r] = isfield(attr, field)

if (isscalar(field))
    tag = field;
else
    tag = dicomlookupint(field);
end

if (isempty(tag))
    r = false;
    return
end

r = attr.info.contains(tag);

end

