function [f] = fieldnames (attr)

tags = attr.info.tags();

f = cell(length(tags),1);

for I=1:length(tags)
    tag = tags(I);
    
    t2 = mod(tag,2^16);
    t1 = (tag-t2)/2^16;
    
    name = dicomlookup(t1,t2);
    if (isempty(name))
        error('Dicom tag does not resolve to name');
    end
    f{I} = name;
end

end

