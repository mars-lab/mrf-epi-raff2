function [info] = dicominfo_dcm4che(filename)

loadDcm4che

try
    din = org.dcm4che3.io.DicomInputStream(java.io.BufferedInputStream(java.io.FileInputStream(filename)));
catch
    info = [];
    return
end

% Disable reading of any bulk data
din.setIncludeBulkData(javaMethod('valueOf', 'org.dcm4che3.io.DicomInputStream$IncludeBulkData', 'NO'));

info = din.readDataset(-1,-1);
din.close();

info = dicomattr(info);

end

