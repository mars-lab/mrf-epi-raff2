function display (attr)

% fprintf('%s\n',char(attr.info.toString(1e6,80)));

tags = attr.info.tags();

for I=1:attr.info.size
    tag = tags(I);
    
    t2 = mod(tag,2^16);
    t1 = (tag-t2)/2^16;
    
    name = dicomlookup(t1,t2);
    vr = attr.info.getVR(tag);
    
    if (isempty(name))
        name = dec2hex(tag);
        name = char(padarray(double(name).',8-length(name),double('0'),'pre')).';
    end
    
    if (vr == vr.SQ)
        fprintf('%47s: Sequence[%d]\n', name, attr.info.getSequence(tag).size);
    else
        tmp = mat2str(subsref(attr, substruct('.', name)));
        if (length(tmp) > 70)
            tmp = [tmp(1:70) ' ...'];
        end
        fprintf('%47s: %s\n', name, tmp);
    end
end

fprintf('\n');

end
