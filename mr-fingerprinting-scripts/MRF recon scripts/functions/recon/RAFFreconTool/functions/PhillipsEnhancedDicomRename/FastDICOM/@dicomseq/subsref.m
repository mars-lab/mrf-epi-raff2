function [v] = subsref(seq, S)

if (strcmp(S(1).type,'.'))
    name = S(1).subs;
    
    ind = regexp(name, '^Item_\d+$', 'once');
    
    if (isempty(ind))
        error('Sequence must be indexed with Item_<number>')
    end
    
    ind = str2double(name(6:end));
    
elseif (strcmp(S(1).type,'()'))
    ind = S(1).subs;
    if (numel(ind) > 1 || numel(ind{1}) > 1)
        error('Only single index allowed for dicomseq')
    end
    ind = ind{1};
else
    error('No {} indexing for dicomseq allowed');
end

v = dicomattr(seq.info.get(ind-1));

if (length(S)>1)
    v = subsref(v, S(2:end));
end

end

