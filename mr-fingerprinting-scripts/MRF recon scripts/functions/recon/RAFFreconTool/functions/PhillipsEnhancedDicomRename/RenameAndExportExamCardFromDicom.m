function  RenameAndExportExamCardFromDicom (name_in, rename, export, recursive, mat, txt)
% Syntax: RenameAndExportExamCardFromDicom (name_in, rename, export, recursive, mat, txt)
%
% Created by Frank Zijlstra   (frank@isi.uu.nl) &
%            Mike van Rijssel  (mike@isi.uu.nl)
% Last Modification Date: 09-09-2016
%
% Renames Enhanced MR DICOM files and Extracts ExamCards into DICOM folder.
% How to run this script:
% 1) Add 'Dependencies' folder to MATLAB path.
% 2) Call this function. Syntax: RenameAndExportExamCardFromDicom (name_in, rename, export, recursive)
%
% Description of input parameters:
% name_in is a STRING of the path to the file or folder that you want to rename and/or search for DICOM-wrapped ExamCards.
% rename is a BOOLEAN that toggles the renaming option.
% export is a BOOLEAN that toggles the ExamCard extraction.
% recursive is a BOOLEAN that determines whether the search is recursive (includes subfolders).
% mat is a BOOLEAN that toggles exporting of scan (EX_ & IF_) parameters as a .mat file
% txt is a BOOLEAN that toggles exporting of scan (EX_ & IF_) parameters as a .txt file

global UICONTINUE;
in_ui = true;
if isempty(UICONTINUE)
    UICONTINUE = true;
    in_ui = false;
end

if (nargin < 2)
    rename = true;
    export = true;
    recursive = false;
    mat = true;
    txt = false;
elseif (nargin < 3)
    export = true;
    recursive = false;
    mat = true;
    txt = false;
elseif (nargin < 4)
    recursive = false;
    mat = true;
    txt = false;
elseif (nargin < 5)
    mat = true;
    txt = false;
elseif (nargin < 6)
    txt = false;
end

if (isempty(rename)); rename = true; end
if (isempty(export)); export = true; end
if (isempty(recursive)); recursive = false; end
if (isempty(mat)); mat = true; end
if (isempty(txt)); txt = false; end

if (~rename && ~export && ~(mat||txt))
    error('Really? No rename & no export? REALLY?! F*** that shit, I''m going home!')
end

dicomdict('set', 'dicom-dict-philips.txt');


if (isdir(name_in))
    foldername = name_in;
    processDirectory(foldername,rename,export,recursive,mat,txt,in_ui);
elseif (exist(name_in,'file') && isDicom(name_in))
    processDicom(name_in,rename,export,mat,txt)
else
    error('Cannot open %s, not a DICOM file or file does not exist...',name_in);
end

end

function [result] = isDicom (filename)
fp = fopen(filename, 'rb');
fseek(fp,128,'bof');
hdr = fread(fp,4,'*char')';
fclose(fp);

result = strcmp(hdr, 'DICM');
end

function processDicom(name_in,rename,export,mat,txt)
[foldername, filename] = fileparts(name_in);
info = dicominfo_dcm4che(name_in);

if strcmp(filename,'DICOMDIR')
    disp(['DICOMDIR file, not processing: ' name_in]);
else
    if  rename
        name_out = renameDicom(info,name_in);
    else
        disp(['Not renaming: ' name_in]);
        name_out = name_in;
    end
    
    if (export && isfield(info, 'ProtocolName') && strcmp(info.ProtocolName, 'ExamCard'))
        fprintf('Extracting ExamCard from: %s ...\n', name_out);
        exportExamCard(info,foldername);
    elseif ((mat||txt)) && ((strcmp('1.2.840.10008.5.1.4.1.1.66',info.SOPClassUID)|| strcmp('1.3.46.670589.11.0.0.12.2',info.SOPClassUID  )))
        if txt
            XXread(name_out);
        end
        
        if mat
            XXinfo = XXread(name_out);
            [p,n,~] = fileparts(name_out);
            XXname = fullfile(p,[n,'.mat']);
            disp(['Saving scan parameters to: ' XXname])
            save(XXname,'XXinfo');
        end
    else
        disp(['Not extracting anything from: ' name_out]);
    end
end

end

function processDirectory (foldername,rename,export,recursive,mat,txt,in_ui)
global UICONTINUE
fprintf('Processing directory: %s ...\n', foldername);
dirList = dir(foldername);

for I=1:length(dirList)
    if (in_ui); drawnow; end
    if UICONTINUE
        if (strcmp(dirList(I).name,'.') || strcmp(dirList(I).name,'..'))
            continue;
        end
        fName = fullfile(foldername, dirList(I).name);
        
        if (isdir(fName))
            if (recursive)
                processDirectory(fName,rename,export,recursive,mat,txt,in_ui);
            end
            
            continue;
        end
        
        if (~isDicom(fName))
            disp(['Not a DICOM file: ' fName])
            continue;
        end
        
        try
            processDicom(fName,rename,export,mat,txt)
        catch ME
            warning(ME.message);    % Suppress errors in order to continue to next file
        end
    end
end
end

function exportExamCard (info,foldername)
blob = typecast(info.MRBlobDataObjectArray.Item_1.MRBlobData, 'uint8');
endIndex = find(blob == 0, 1);

tmpName = [tempname '.xml'];

fp = fopen(tmpName, 'w');
fwrite(fp, blob(1:endIndex-1));
fclose(fp);

data = xmlread(tmpName);

delete(tmpName);

xmlTags = data.getDocumentElement.getChildNodes;
for I=1:xmlTags.getLength
    if (strcmp(xmlTags.item(I).getNodeName, 'ExamCardBlob'))
        examcardData = xmlTags.item(I).getChildNodes.item(0).getData;
        
        blob2 = org.apache.commons.codec.binary.Base64.decodeBase64(uint8(char(examcardData)));
        endIndex = find(blob2 == 0, 1);
        
        try
            name = info.ExamCardName;
        catch
            try
                name = info.StudyDescription;
            catch
                name = 'EXCEED_extracted';
            end
        end
        
        if (isempty(name))
            name = info.StudyDescription;
        end
        fp = fopen(fullfile(foldername, [name '.ExamCard']), 'w');
        fwrite(fp, blob2(1:endIndex-1));
        fclose(fp);
        
        break
    end
end
end

function filename_new = renameDicom(info,filename_orig)
if strcmp('1.2.840.10008.5.1.4.1.1.4.1',info.SOPClassUID) || ...                        % Enhanced MR image
        strcmp('1.2.840.10008.5.1.4.1.1.66',info.SOPClassUID) || strcmp('1.3.46.670589.11.0.0.12.2',info.SOPClassUID)  % XX_ parameter OR ExamCard
    
    % get data from header
    PatID = info.PatientID;
    scannr = num2str(info.MRSeriesAcquisitionNumber,'%02d');
    reconnr = num2str(info.MRSeriesReconstructionNumber,'%02d');
    
    if ~(isfield(info, 'ProtocolName') && strcmp(info.ProtocolName, 'ExamCard'))        % NO ExamCard
        scanname = info.SeriesDescription;
        scanname = regexprep(scanname, '[\\/:*?"<>|]','_');
    end
    
    % construct name
    foldername = fileparts(filename_orig);
    if strcmp('1.2.840.10008.5.1.4.1.1.4.1',info.SOPClassUID)                           % Enhanced MR image
        %             filename_new = fullfile(foldername, [ PatID '_' scannr '_' reconnr '_' scanname '.dcm']);
        filename_new = fullfile(foldername, [PatID '_' scannr '_' reconnr '_' scanname '.dcm']);
    elseif (isfield(info, 'ProtocolName') && strcmp(info.ProtocolName, 'ExamCard'))     % ExamCard
        filename_new = fullfile(foldername, [PatID '_' scannr '_' reconnr '_ExamCard.dcm']);
    else                                                                                % XX_ parameter
        filename_new = fullfile(foldername, [PatID '_'  scannr '_' reconnr '_' scanname '_XXscanpars']);
    end
    
    % rename file
    if(~strcmpi(filename_orig,filename_new))
        disp(['Renaming: ' filename_orig ' to ' filename_new]);
        movefile(filename_orig, filename_new);
    else
        disp(['Not renaming: ' filename_orig]);
    end
else
    filename_new = filename_orig;
    disp(['Unsupported DICOM, not renaming: ' filename_orig]);
end
end