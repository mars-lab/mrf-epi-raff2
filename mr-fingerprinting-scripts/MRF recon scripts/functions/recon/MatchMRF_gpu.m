function [MaxCorrVal, CorrelationCoeff] = MatchMRF_gpu(D, MRFBilder,N_unix)

sz = 0;

if length(size(MRFBilder)) == 2
    [a1,a2] = size(MRFBilder);
    M = MRFBilder;
    sz = 2;
elseif length(size(MRFBilder)) == 3
    [a1,a2,a3] = size(MRFBilder);
    M = reshape(MRFBilder, a1*a2, a3);
    sz = 3;
else
    disp('Wrong dimensions')
end

[dd1,dd2] = size(M);

D = single(abs(D));
D = D./repmat(sqrt(sum(D.^2,2)), 1,dd2);
D=gpuArray(D);
M = single(abs(M));
M = M./repmat(sqrt(sum(M.^2,2)), 1, dd2);
M=gpuArray(M);
% calculate max memory (a bit of a guess, but better than nothing)
CorrelationCoeff = zeros(dd1,1);
MaxCorrVal = ones(dd1,1);
CorrelationCoeff_gpu=gpuArray(CorrelationCoeff);
MaxCorrVal_gpu=gpuArray(MaxCorrVal);

if ispc
    mem = memory;
    N = round(mem.MaxPossibleArrayBytes./(2*4*length(D)*1.7)/2);
    N_loop = ceil(dd1/N);
    for jloop = 1:ceil(dd1/N)
        idx = (jloop-1)*N+1:min(jloop*N, dd1);
        [CorrelationCoeff_gpu(idx), MaxCorrVal_gpu(idx)] = max(D*M(idx,:)');
    end
end

if isunix
    N = N_unix;
    N_loop = ceil(dd1/N);
    
    for jloop = 1:ceil(dd1/N)
        %disp(num2str(jloop/N_loop)); 
        idx = (jloop-1)*N+1:min(jloop*N, dd1);
        
        [CorrelationCoeff_gpu(idx), MaxCorrVal_gpu(idx)] = max(D*M(idx,:)');
    end
end

if sz == 3
    CorrelationCoeff_gpu = reshape(CorrelationCoeff_gpu, a1, a2);
    MaxCorrVal_gpu = reshape(MaxCorrVal_gpu, a1, a2);
end
MaxCorrVal=gather(MaxCorrVal_gpu);
CorrelationCoeff=gather(CorrelationCoeff_gpu);
end