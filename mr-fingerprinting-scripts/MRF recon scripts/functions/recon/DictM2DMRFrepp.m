function [dict] = DictM2DMRFrepp(RFpulses, RefDic ,TR , TE,slice_order,extra_inversion,prescan_wait,REPP_times,N_no_SL,SL_wait,M2D_jump,extra_REPP_fact,REPP_in_2nd)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% check if gpu is available
try
    canUseGPU = parallel.gpu.GPUDevice.isAvailable;
catch ME
    canUseGPU = false;
end
disp(['Using GPU: ' num2str(canUseGPU)]);
% fat surpresion
T_spir=16.5;
T_fatsur=T_spir; 

M2D_jump=double(M2D_jump);
SL_ind=1;
RefDic(:,RefDic(1,:) < RefDic(2,:)) =  [];

T1 = RefDic(1,:);
T2star = RefDic(2,:);
AngleCorrection = RefDic(4,:);
T1rho = RefDic(5,:);
if size(RFpulses,2)~=size(AngleCorrection,1)
    RFpulses=RFpulses';
end
rf = abs(RFpulses*AngleCorrection);  % acount for angle correction

N_slice=length(slice_order);
extra_REPP_fact=double(extra_REPP_fact);
effective_N_slice=max(floor(N_slice/extra_REPP_fact),1);
disp(['Effecttive N slice=', num2str(effective_N_slice)]);
loc_save=0;
Cur_ind_fact=REPP_in_2nd+1;

d1 = size(RefDic,2);    % number of fingerprints
d2 = length(RFpulses);  % length of fingerprints
dict = zeros(d1,d2,N_slice);    % output dictionary
N_dyn=d2;
if canUseGPU
    M0 = gpuArray(zeros(3,d1,N_slice));       % temporarz magnetization vector, [mx,my,mz] (3*N_dyn*N_slice)
else
    M0 = (zeros(3,d1,N_slice));
end
disp(['M0 gpu array:' num2str(isa(M0,'gpuArray'))])
M0(3,:,:) = 1;

REPP_train_length=(N_dyn-N_no_SL)/length(REPP_times);

for dyn=1:N_dyn
    for loc=1:N_slice
        slice=slice_order(loc);
        if dyn==1 && loc==1
            if prescan_wait==0
                %Prescan on first slice if no wait
                %Flip
                
                M0(:,:,slice) = rx_flip3D(M0(:,:,slice) , rf(1,:) );
                % relax by TE
                M0(:,:,slice) = relx3D(M0(:,:,slice) , TE(1), T1, T2star);
                %no readout
                % relax
                M0(:,:,slice) = relx3D(M0(:,:,slice) , TR(1) - TE(1), T1, T2star);
                % spoil transversal magnatization
                M0(1:2,:,slice) = 0;
            end
            %%%%%% invert all slices
            for k=1:size(M0,3)
                M0(:,:,k) = rx_flip3D(M0(:,:,k), pi);
%                 M0(:,:,k) = relx3D(M0(:,:,k), 12, T1, T2star);
                M0(1:2,:,k) = 0;
            end
            %%%%% Acquire current slice and relax others
            % Relax by fat sur
            M0(:,:,slice) = relx3D(M0(:,:,slice) , T_fatsur, T1, T2star);% relax by TE
            %Flip
            M0(:,:,slice) = rx_flip3D(M0(:,:,slice) , rf(1,:));
            M0(:,:,slice) = relx3D(M0(:,:,slice) , TE(dyn), T1, T2star);% relax by TE
            
           if canUseGPU
                dict(:,dyn,slice)=gather((M0(1,:,slice)+1i.*M0(2,:,slice))');% readout
            else
                dict(:,dyn,slice)=((M0(1,:,slice)+1i.*M0(2,:,slice))');% readout
            end
            M0(:,:,slice) = relx3D(M0(:,:,slice) , TR(dyn) - TE(dyn)-T_fatsur, T1, T2star);% relax
            M0(1:2,:,slice) = 0;% spoil transversal magnatization
            for k=setdiff(1:N_slice,slice)
                M0(:,:,k) = relx3D(M0(:,:,k) , TR(dyn), T1, T2star);
            end %relax all others
        end
        if dyn==1 && loc~=1
            %%%%% Pre scan current slice and relax others
            % Relax by fat sur
            M0(:,:,slice) = relx3D(M0(:,:,slice) , T_fatsur, T1, T2star);% relax by fat sur
            %Flip
            M0(:,:,slice) = rx_flip3D(M0(:,:,slice) , rf(dyn,:));
            M0(:,:,slice) = relx3D(M0(:,:,slice) , TE(dyn), T1, T2star);% relax by TE
            
            %dict(:,dyn,slice)=(M0(1,:,slice)+1i.*M0(2,:,slice))';% readout
            M0(:,:,slice) = relx3D(M0(:,:,slice) , TR(dyn) - TE(dyn)-T_fatsur, T1, T2star);% relax
            M0(1:2,:,slice) = 0;% spoil transversal magnatization
            for k=setdiff(1:N_slice,slice)
                M0(:,:,k) = relx3D(M0(:,:,k) , TR(dyn), T1, T2star);
            end
            
            %%%%% Acquire current slice and relax others
            
            %Flip
            % Relax by fat sur
            M0(:,:,slice) = relx3D(M0(:,:,slice) , T_fatsur, T1, T2star);% relax by fat sur
            M0(:,:,slice) = rx_flip3D(M0(:,:,slice) , rf(1,:));
            M0(:,:,slice) = relx3D(M0(:,:,slice) , TE(dyn), T1, T2star);% relax by TE
            
            if canUseGPU
                dict(:,dyn,slice)=gather((M0(1,:,slice)+1i.*M0(2,:,slice))');% readout
            else
                dict(:,dyn,slice)=((M0(1,:,slice)+1i.*M0(2,:,slice))');% readout
            end
            M0(:,:,slice) = relx3D(M0(:,:,slice) , TR(dyn) - TE(dyn)-T_fatsur, T1, T2star);% relax
            M0(1:2,:,slice) = 0;% spoil transversal magnatization
            for k=setdiff(1:N_slice,slice)
                M0(:,:,k) = relx3D(M0(:,:,k) , TR(dyn), T1, T2star);
            end
        end
        
        if dyn ~=1
            if extra_inversion==1 && mod((dyn-1),2)==0 && ((dyn-1)*2==(loc-1)) %% invert if conditions are met
                disp(strcat('extra inversion pulse in dyn=',num2str(dyn), ' ,loc =', num2str(loc), 'slice =', num2str(slice)))
                %%%%%% invert all slices
                for k=1:size(M0,3)
                    M0(:,:,k) = rx_flip3D(M0(:,:,k), pi);
                    M0(:,:,k) = relx3D(M0(:,:,k), 12, T1, T2star);
                    M0(1:2,:,k) = 0;
                end
            end
            %%%%% Acquire current slice and relax others
            
            %Flip
            %% if (((SL_index*MPF_ACQ->MRF_T1r_M2D_jump)% MPF->packages)==loc_ix)
            if(dyn-N_no_SL)>0
                SL_index = (((dyn-1) - N_no_SL) / REPP_train_length)+1;
                cur_ind=(((SL_index-1)*Cur_ind_fact))*M2D_jump;
                
                %if ((IFFE->dynamic_scan - MPF_ACQ->MRF_N_no_t1rho) % MPF_ACQ->MRF_T1rho_train_length == 0)
                %                 if (mod((dyn-1)-N_no_SL,REPP_train_length)==0)
                if (mod((dyn-1)-N_no_SL,REPP_train_length)==0)
                    if mod(cur_ind,effective_N_slice)==(loc-1)
                        %%%%%% apply RAFF to all slices
                        disp(['repp: ',num2str(SL_index),'in' ,num2str(dyn),', loc:', num2str(loc)])
                        loc_save=loc;
                        for k=1:size(M0,3)
                            M0(:,:,k) = relx3D(M0(:,:,k) , double(SL_wait), T1, T2star);% relax
                            M0(:,:,k) = M0(:,:,k).*exp(-REPP_times(SL_index)./T1rho);
                            M0(1:2,:,k) = 0;
                        end
                    end
                    if extra_REPP_fact>1
                        for fact_ind=1:extra_REPP_fact-1
                            if (fact_ind*effective_N_slice)+loc_save==loc
                               disp(['extra repp: ',num2str(SL_index),'in' ,num2str(dyn),', loc:', num2str(loc)])
                                for k=1:size(M0,3)
                                    
                                    M0(:,:,k) = M0(:,:,k).*exp(-REPP_times(SL_index)./T1rho);
                                    M0(1:2,:,k) = 0;
                                end
                            end
                        end
                    end
                end
                if REPP_in_2nd==1
                    if (mod((dyn-1)-N_no_SL,REPP_train_length)==1)
                        SL_index = (((dyn-2) - N_no_SL) / REPP_train_length)+1;
                        cur_ind=(((SL_index-1)*Cur_ind_fact)+1)*M2D_jump;
                        if mod(cur_ind,effective_N_slice)==(loc-1)
                            %%%%%% apply RAFF to all slices
                            disp(['repp: ',num2str(SL_index),' in:' ,num2str(dyn),', loc:', num2str(loc)])
                            loc_save=loc;
                            for k=1:size(M0,3)
                                M0(:,:,k) = relx3D(M0(:,:,k) , double(SL_wait), T1, T2star);% relax
                                M0(:,:,k) = M0(:,:,k).*exp(-REPP_times(SL_index)./T1rho);
                                M0(1:2,:,k) = 0;
                            end
                        end
                        if extra_REPP_fact>1
                            for fact_ind=1:extra_REPP_fact-1
                                if (fact_ind*effective_N_slice)+loc_save==loc
                                   disp(['extra repp: ',num2str(SL_index),'in' ,num2str(dyn),', loc:', num2str(loc)])
                                    for k=1:size(M0,3)
                                        
                                        M0(:,:,k) = M0(:,:,k).*exp(-REPP_times(SL_index)./T1rho);
                                        M0(1:2,:,k) = 0;
                                    end
                                end
                            end
                        end
                    end
                end
            end
            M0(:,:,slice) = relx3D(M0(:,:,slice) , T_fatsur, T1, T2star);% relax by fat surpresion time
            M0(:,:,slice) = rx_flip3D(M0(:,:,slice) , rf(dyn,:));
            M0(:,:,slice) = relx3D(M0(:,:,slice) , TE(dyn), T1, T2star);% relax by TE
            
            if canUseGPU
                dict(:,dyn,slice)=gather((M0(1,:,slice)+1i.*M0(2,:,slice))');% readout
            else
                dict(:,dyn,slice)=((M0(1,:,slice)+1i.*M0(2,:,slice))');% readout
            end
            M0(:,:,slice) = relx3D(M0(:,:,slice) , TR(dyn) - TE(dyn)-T_fatsur, T1, T2star);% relax
            M0(1:2,:,slice) = 0;% spoil transversal magnatization
            for k=setdiff(1:N_slice,slice)
                M0(:,:,k) = relx3D(M0(:,:,k) , TR(dyn), T1, T2star);
            end
            
        end
    end
    
end
end

