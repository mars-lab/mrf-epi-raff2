function Map=generateMRFmap(filename,XXinfo,denoise,denoise_kernel)
%% Generates a MRF EPI Map of both T1 and T2* based on the
% MARS-Lab MRF EPI implementation by Telly Ploem. The script assumes some
% parameters are saved in a XX file and stored. You should use this code
% together with CreateScanOverview.

[data_rescaled, MRF_dicom_info] = loadPhillipsDicom(filename);
if denoise==1
    %% reshape data for denoising

dyn_list=MRF_dicom_info.dyn_slice(3,:);
sl_list=MRF_dicom_info.dyn_slice(2,:);
[a1,a2,a3]=size(data_rescaled);
data_rescaled_reshape=reshape(data_rescaled,a1,a2,max(dyn_list),max(sl_list));

%%
disp('denoising baseline images')
data_denoise=MPdenoising(data_rescaled_reshape,[],denoise_kernel);


data_rescaled = reshape(data_denoise,a1,a2,a3);
    
end
N_slice = double(max(MRF_dicom_info.dyn_slice(2,:)));
N_dyn = MRF_dicom_info.NumberDyamics;
FA = double(XXinfo.EX_MDME_MRF_fa_save);
FA = FA(1:N_dyn);
FA(FA == -1) = [];
TE = double(XXinfo.EX_MDME_MRF_te_save);
TE = TE(1:N_dyn);
TE(TE == -1) = [];
extra_inversion = XXinfo.EX_ACQ_MRF_MS_extrainversion;

try
    break_MRF = XXinfo.CSC_MRF_break_before_acq;
catch
    break_MRF = 0; %in  older patches we didnt have a break
end

pre_scan = not(break_MRF);

%% TE, TR and FA
TR_base = MRF_dicom_info.TR;
TE_base = MRF_dicom_info.TE;
TR = TR_base + TE;
TE = TE + TE_base;

%% Calc slice order
clear Slice_order
interleav_N = round(sqrt(N_slice));
ind = 0;
j = 0;

for i = 0:N_slice-1
    slice_num = interleav_N*j+ind;
    
    if slice_num > N_slice-1
        j = 0;
        ind = ind+1;
        slice_num = interleav_N*j+ind;
    end
    
    Slice_order(i+1) = slice_num;
    j = j+1;
end

Slice_order = Slice_order+1;

%% dict values
clear T1List
clear T2starList

T1List(1) = 1;
count = 2;
tmpV = 20;

while (tmpV < 3500)
    T1List(count) = tmpV;
    tmpV = tmpV.*1.05;
    count = count+1;
end

MRF_input.T1 = T1List;
T2starList(1) = 1;
count = 2;
tmpV = 5;

while (tmpV < 300)
    T2starList(count) = tmpV;
    tmpV = tmpV.*1.05;
    
    count = count+1;
end

MRF_input.T2star = T2starList;

MRF_input.AngleCorrectList = 0.6:0.1:1.2;

MRF_input.OffResList=0;  %List of off-resonance values to be simulated [Hz]

RefDic = combvec(MRF_input.T1, MRF_input.T2star, MRF_input.OffResList, MRF_input.AngleCorrectList);
RefDic(:,RefDic(1,:) < RefDic(2,:)) = [];

RFpulses = FA/180*pi;

%% create dict and match
tic
disp(['Creating dictionary'])
[dict] = DictMultiSlice(RFpulses, RefDic ,TR , TE,N_slice,Slice_order,extra_inversion,pre_scan);
D = abs(dict);

Map.T1              = zeros(size(data_rescaled,1), size(data_rescaled,2), N_slice);
Map.T2star          = zeros(size(data_rescaled,1), size(data_rescaled,2), N_slice);
Map.AngleCorrection = Map.T1;

for slice =1:N_slice
    disp(['Matching slice ' num2str(slice) '/' num2str(N_slice)])
    dyn_slice = MRF_dicom_info.dyn_slice;
    slice_index = dyn_slice(2,:);
    image = data_rescaled(:,:, slice_index==slice);
    
    [MaxCorrVal, CorrelationCoeff] = MatchMRF(D(:,:,slice),image);
    
    for i = 1:size(image,1)
        for j = 1:size(image,2)
            Map.T1(i,j,slice) = RefDic(1,(MaxCorrVal(i,j)));
            Map.T2star(i,j,slice) = RefDic(2,(MaxCorrVal(i,j)));
            Map.AngleCorrection(i,j,slice) = RefDic(4,(MaxCorrVal(i,j)));
        end
    end
    
    Map.CorrelationCoeff(:,:,slice) = CorrelationCoeff;
    Map.MaxCorrVal(:,:,slice) = MaxCorrVal;
end

Map.dicom_info = MRF_dicom_info;
Map.XXinfo = XXinfo;

toc
end


