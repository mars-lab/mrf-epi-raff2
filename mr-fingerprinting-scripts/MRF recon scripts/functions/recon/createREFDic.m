function [RefDic] = createREFDic(MRF_REPP,increment)
%CREATEREFDIC Creates a RefDic containing all possible combinatations in
%the dictionary 
%   MRF_REPP Bool that enables an extra Trepp term (T1rho or Traff)
%   increment [double] increment in which to increase the dict entries in %
T1_range=[100 3500];
T2s_range=[10 400];
Trepp_range=[10 400];

incr_fact=1+increment/100;


MRF_input.AngleCorrectList = 0.6:0.1:1.2;

clear T1List
T1List(1) = T1_range(1);
count = 2;
tmpV = T1List(1)*incr_fact;
while (tmpV < T1_range(2))
    T1List(count) = tmpV;
    tmpV=tmpV.*incr_fact;
    count = count+1;
end

 %T1List=linspace(100,4e3,2);


MRF_input.T1=T1List;

clear T2starList
T2starList(1) = T2s_range(1);
count = 2;
tmpV = T2starList(1)*incr_fact;
while (tmpV < T2s_range(2))
    T2starList(count) = tmpV;
    tmpV=tmpV.*incr_fact;
    count = count+1;
end

MRF_input.T2star=T2starList;


MRF_input.OffResList=0;  %List of off-resonance values to be simulated [Hz]end
clear Trepplist
if MRF_REPP==1
Trepplist(1) = Trepp_range(1);
count = 2;
tmpV = Trepplist(1)*incr_fact;
while (tmpV < Trepp_range(2))
    Trepplist(count) = tmpV;
    tmpV=tmpV.*incr_fact;
    count = count+1;
end
else
    Trepplist=1;
end
MRF_input.Trepp=Trepplist;


RefDic = combvec(MRF_input.T1,MRF_input.T2star,MRF_input.OffResList, MRF_input.AngleCorrectList,MRF_input.Trepp);
RefDic(:,RefDic(1,:) < RefDic(2,:)) =  [];
end

