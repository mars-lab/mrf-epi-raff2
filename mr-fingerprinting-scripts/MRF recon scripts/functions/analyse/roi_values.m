function [ROI_val] = roi_values(image,centers,radiiSize)
%UNTITLED2 Summary of this function goes here
%   centers array N*2 
 if nargin < 2
  error('input_example :  data is a required input')
end

if nargin < 3
  radiiSize=5;
end
centers=round(centers);
% radiiSize = 10;
ROI_val=zeros(size(centers,1),2);

for vial=1:size(centers,1)
    mask = createCirclesMask(image, centers(vial,:), radiiSize);
    ROI_val(vial,1)= mean(image(mask));
    ROI_val(vial,2)= std(image(mask));
    
end

