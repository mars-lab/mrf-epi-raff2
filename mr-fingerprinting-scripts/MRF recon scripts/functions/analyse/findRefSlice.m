function [slice] = findRefSlice(XXinfo_MRF,XXinfo_fit)
%FINDREFSLICE Calculates the slice position of the Ref scan wrt to the MRF
%scan
%   Detailed explanation goes here
MRF_offcent=XXinfo_MRF.EX_GEO_cur_stack_lr_offcentre;
fit_offcent=XXinfo_fit.EX_GEO_cur_stack_lr_offcentre;
offset=ceil(double((XXinfo_MRF.EX_GEO_cur_stack_slices))/2);
slice=round(((MRF_offcent-fit_offcent)/3.3))+offset;
end

