function stats=showOverlay(baseImage,overlay,mask)
baseImage = baseImage/(max(baseImage(:))); % normalize base (anatomical) image
baseImage  = baseImage(:,:,[1 1 1]);        % converting to RGB (ignore colormaps)
imshow(overlay, []);             % show parametric image
colormap();                           % apply colormap
hold on;
h = imshow(baseImage);                      % superimpose anatomical image
set(h, 'AlphaData', ~mask);      % make pixels in the ROI transparent
colorbar();     
hold off
stats(1,1)=mean(overlay(mask>0));
stats(1,2)=std(overlay(mask>0));
end
