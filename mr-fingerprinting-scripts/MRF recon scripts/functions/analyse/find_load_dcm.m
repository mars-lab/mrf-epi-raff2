function [image,image_info]=find_load_dcm(name,all_files_folder)
%FIND_LOAD_DCM helper function to find and load the correct dcm based on
%filename
%   Detailed explanation goes here

if nargin<2
    all_files_folder='/Volumes/T7 Touch/MEP analysis/**/*.dcm';
end
    
all_files = dir(all_files_folder);
all_files = all_files(~startsWith({all_files.name}, '.'));
[~,name,~]=fileparts(name);
if contains(name,'_XXscanpars')
file_ind=find(contains({all_files.name},extractBefore(name,'_XXscanpars')));
else
    file_ind=find(contains({all_files.name},name));
end
if length(file_ind)>1
    file_ind=file_ind(1);
end
file_name=fullfile(all_files(file_ind).folder,all_files(file_ind).name);

[image,image_info]=loadPhillipsDicom(file_name);



end

