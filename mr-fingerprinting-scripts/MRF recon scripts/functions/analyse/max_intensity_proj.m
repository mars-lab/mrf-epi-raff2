function image_pr=max_intensity_proj(image,dcm_info)


sl_ind=dcm_info.dyn_slice(2,:);
N_slice=max(sl_ind);
for sl=1:N_slice
    image_pr(:,:,sl)=max(image(:,:,sl_ind==sl),[],3);
end

end