function mask_tot=mask_MRF(Map,image_pr,corr_ll,int_lim_tot,int_lim_loc)
%%
% suggested defaults:
% corr_ll=0.96;
% int_lim_tot=0.03; %image intensity rel to max all slices
% int_lim_loc=0.08; % relative int on slice
int_mask_tot=image_pr>int_lim_tot*max(image_pr,[],'all');
int_mask_loc=image_pr>int_lim_loc*max(image_pr,[],[1 2]);
int_mask_corr=Map.CorrelationCoeff>corr_ll;
mask_tot=double(int_mask_tot).*double(int_mask_loc).*double(int_mask_corr);

end
