function mask_tot=mask_fit(image_pr,lim)
  % recomended lim:
  % T1, RAFF =0.3
  %T2s = 0.4
mask_tot=double(image_pr>lim*max(image_pr,[],'all'));

end