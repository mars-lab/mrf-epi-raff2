function FitResults=RAFF_fit(image,XXinfo,third_param)
%Fits RAFF maps from image series created by RAFF patch of Telly Ploem
%image= image series (N_x,N_y,N_t)
%XXinfo= XXinfo file containing all EX params
%third param= param to set 3 param fit (1) or 2 param fit (0)
if nargin<3
    third_param=0;
end
RAFF_blocks=XXinfo.EX_MDME_MRF_RAFF_save;
RAFF_blocks=double(RAFF_blocks(RAFF_blocks~=-1));
Tp=(2*1000)/(sqrt(2)*double(XXinfo.EX_REPP_RAFF_freq));
RAFF_times=RAFF_blocks.*Tp;

magn_data=image(:,:,1:length(RAFF_times));
magn_data=magn_data./max(magn_data,[],3);
fit_im=zeros(size(magn_data,1),size(magn_data,2),1,size(magn_data,3));
fit_im(:,:,1,:)=magn_data;
mask=ones(size(magn_data,1),size(magn_data,2));

data = struct();
Model = mono_t2;
Model.Prot.SEdata.Mat = [ RAFF_times ];
Model.options.OffsetTerm=third_param;

data.SEdata=fit_im;

data.Mask=mask;
gcp;
FitResults = ParFitData(data,Model);
FitResults.XXinfo=XXinfo;


end


