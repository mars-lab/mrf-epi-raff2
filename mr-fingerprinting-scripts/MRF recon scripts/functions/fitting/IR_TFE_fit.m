function FitResults=IR_TFE_fit(image,XXinfo)
%normalize and reshape image
sz=size(image);
data = struct();
if length(sz)==3
    norm_image=image./max(image,[],3);
    data.IRData=zeros(sz(1),sz(2),1,sz(3));
    data.IRData(:,:,1,:)= double(norm_image);
    N_dyn=sz(3);
end
if length(sz)==4
    norm_image=image./max(image,[],4);
    data.IRData=norm_image;
    N_dyn=sz(4);
end
if length(sz)<3
    error('no time dimension in data')
end



%setup model
Model = inversion_recovery;
TI=double(XXinfo.EX_TFEPP_IR_TFE_times);
TI=TI(1:N_dyn);
TR=XXinfo.EX_TFE_interval;

Model.Prot.IRData.Mat = [ TI];
Model.Prot.TimingTable.Mat = [ TR];
Mask=ones(size(image,[1 2]));
data.Mask= double(Mask);
gcp;
FitResults = ParFitData(data,Model);
FitResults.XXinfo=XXinfo;
[filepath,name,ext] = fileparts(XXinfo.Filename);
% out_dir=[filepath,'/fit'];
% mkdir(out_dir);
% save([out_dir,'/T1_FitResults_',name],'FitResults');




end