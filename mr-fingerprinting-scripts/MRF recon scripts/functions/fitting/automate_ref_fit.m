tic()
clear refMaps
MRFdir=uigetdir('open dicom folder');
disp('Start renaming files')
% RenameAndExportExamCardFromDicom (MRFdir, 1, 1, 0, 1, 0) %non recursive folder search you can change this
%%
files=scanInfo(MRFdir);


% files=scanInfo(MRFdir);
j=1;
for i=1:length(files)
    try
        filename=fullfile(files(i).folder,files(i).name);
        XXinfo=files(i).XXinfo;
        
        if XXinfo.EX_REPP_RAFF_mapping==1
            image=loadPhillipsDicom(filename);
            FitResults=RAFF_fit(image,XXinfo);
            [filepath,name,ext] = fileparts(XXinfo.Filename);
            disp('RAFF');
            
            refMaps(j).name=files(i).name;
            refMaps(j).type='RAFF';
            refMaps(j).dcm=image;
            refMaps(j).map=FitResults.T2;
            refMaps(j).map(refMaps(j).map>300)=300;
            refMaps(j).FitResults=FitResults;
            refMaps(j).XXinfo=XXinfo;
            j=j+1;
        end
        if XXinfo.EX_ACQ_MRF_IR_TFE==1
            image=loadPhillipsDicom(filename);
            FitResults=IR_TFE_fit(image,XXinfo);
            
            
            refMaps(j).name=files(i).name;
            refMaps(j).type='T1';
            refMaps(j).dcm=image;
            refMaps(j).map=FitResults.T1;
            refMaps(j).map(refMaps(j).map>3e3)=3e3;
            refMaps(j).FitResults=FitResults;
            refMaps(j).XXinfo=XXinfo;
            disp('T1');
            j=j+1;
        end
        if  XXinfo.EX_PROC_func_types(1)==1
            image=loadPhillipsDicom(filename);
            if size(image,3)>2
                map=image(:,:,end-1);
                refMaps(j).name=files(i).name;
                refMaps(j).type='T2s';
                refMaps(j).dcm=image;
                refMaps(j).map=map;
                refMaps(j).FitResults=map;
                refMaps(j).XXinfo=XXinfo;
                j=j+1;
            end
        end
    catch e
        fprintf(1,'The identifier was:\n%s',e.identifier);
        fprintf(1,'There was an error! The message was:\n%s',e.message);
    end
end

%%
save('refMaps','refMaps');
toc()