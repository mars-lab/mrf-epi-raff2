function [fit] = MapT1rho_2(SL_times,data)
%MAPT1RHO Creates a T1rho mapping 2 parameter fit
%SL times should be an vector of SL times
%data should be (nx,ny,SL_times) matrix
%  

%% prep data
sz=size(data);
mapping_data_norm=data./max(data,[],3);

%% Map

mapping_data_norm(isnan(mapping_data_norm))=0;
data=reshape(mapping_data_norm(:,:,:),[sz(1)*sz(2),sz(3)]);
xdata_ND=double(repmat(SL_times',sz(1)*sz(2),1));
x0_ND=repmat([1 40],sz(1)*sz(2),1);
x0_ND(:,1)=max(data,[],2);
% x0_ND(:,3)=min(data,[],2);
f_ND = @(x,xdata) x(:,1).*exp(-xdata./x(:,2));
LB_ND=repmat([0 0],sz(1)*sz(2),1);
UB_ND=repmat([700 2],sz(1)*sz(2),1);
N_b=30; %batch size
ax=zeros(size(data,1),2);
tic
N_loop=ceil((sz(1)*sz(2))/N_b);
for i=1:N_loop
    if i<N_loop
        ind=(1+N_b*(i-1)):N_b*i;
    else
        ind=(1+N_b*(i-1)):(sz(1)*sz(2));
    end
ax(ind,:) = lsqcurvefit(f_ND,x0_ND(ind,:),xdata_ND(ind,:),data(ind,:),LB_ND,UB_ND);
disp([(num2str((i/N_loop)*100)) '% done'])
end
toc
% disp(ax(max(ind),2))
T1rho=ax(:,2);
T1rho=reshape(T1rho,sz(1),sz(2));
fit.a=reshape(ax(:,1),sz(1),sz(2));
fit.T1rho=T1rho;
fit.x_data=SL_times;
fit.y_data=mapping_data_norm;


end

