function b=T1_2param_fit(im,IR_times,mask)
if nargin<3
    mask=ones(im);
end
sz=size(im,1,2);
a=zeros(sz);
b=a;
options=optimset('Display','off');
x_f=IR_times;
for x=1:384
%     if mod(x/384,.1)==0
    disp(num2str(x/384,2))
%     end
for y=1:384
if mask(y,x)==1
ydat=squeeze(im(y,x,:));

X0=[ydat(1) 1000];
f=lsqcurvefit(@myfun, X0, x_f, ydat,[],[],options);
a(y,x)=f(1);
b(y,x)=f(2);
% fit(y,x,:)=lsqcurvefit(@myfun, X0, x_f, ydat,[],[],options);
end
end
end

end

function F = myfun(x,xdata)
F= abs(x(1)*(1-2*exp(1).^-(xdata./x(2))));
end
       