function Map=maskMap(Map,mask)
Map.T1(~mask)=0;
Map.T2star(~mask)=0;
Map.Trepp(~mask)=0;
Map.MaxCorrVal(~mask)=0;
Map.CorrelationCoeff(~mask)=0;
Map.AngleCorr(~mask)=0;



end