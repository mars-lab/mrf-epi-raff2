function saveFig(output_name)
saveas(gcf,[output_name]);
exportgraphics(gcf,[output_name,'.png'],'Resolution',700)
saveas(gcf,[output_name,'.svg'])
end