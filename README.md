The scripts in this repository were used to create the parametric maps of:

Joao Tourais, Telly Ploem, Tijmen A. van Zadelhoff, Christal van de Steeg-Henzen, Edwin H. G. Oei, Sebastian Weingartner. Rapid Whole-Knee Quantification of Cartilage using T1, T2*, and TRAFF2 mapping with Magnetic Resonance Fingerprinting. IEEE TRANSACTIONS ON BIOMEDICAL ENGINEERING, VOL. XX, NO. XX, XXXX 2022.

